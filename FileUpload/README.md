# FileUpload

## 介绍
原生js写的文件上传程序，支持单击打开文件选择框上传和将文件拖拽到某一区域上传，版本：v1.0.01

## 安装教程
无需安装，直接在页面中直接使用script标签引用

## 使用说明

### 1.  默认参数
```
let FileUploadDP = { //默认参数
		ClickElement: null, //单击元素
		DragElement: null, //拖拽元素
		OPPattern: null, //操作模式
		ReturnButtom: null, //返回元素
		QuantityLimit: 1024, //数量限制
		ConsoleAutograph: true, //是否开启版权提示
		OutputWarning: true, //是否输出警告
		FormatList: [, 'svg+xml', 'avif', 'bmp', 'gif', 'ico', 'cur','jpg',
         'jpeg', 'jfif', 'pjpeg','pjp','png','svg', 'tif', 'tiff', 'webp',
          'ogg', 'mp3', 'wav', 'flac', 'mpeg', 'mp4', 'webm', 'avi', 'ogv'],
		OPState: function(OPState) {
			console.log("OPState:" + OPState);
		}, //操作状态
		ResultSet: function(ResultSet) {
			console.log(ResultSet);
		} //操作结果
	};
```
### 2.  调用示例
```
FileUpload({
		DragElement: '.UploadDiv',
		ClickElement: '#UploadDiv',
		OPPattern: 'DragandClick',
		ReturnButtom: '.FileDiv>div>input[type="button"]',
		ConsoleAutograph: true,
		OutputWarning: false,
		FormatList: ['png','jpg','jpeg','gif],
		QuantityLimit: 500,
		OPState: function(OPState) {
            console.log("OPState:" + OPState);
        },
        ResultSet:function(ResultSet) {
            console.log(ResultSet);
        }
});
```
### 3.  删除文件示例
```
FileUpload.Delete({
		FileName:（获得的文件名）,
		FileFormat: （获得的文件格式）,
		FileRoute: （获得的文件路径）,
		SubmitTime: （返回的提取文件时的时间戳）,
		lastModified: （文件最后的格式化时间）,
		FileSize: （文件大小）,
		DeleteOPState: function(DeleteOPState) {
            console.log(DeleteOPState);
        },//返回删除操作状态码
        DeleteResultSet: function(DeleteResultSet) {
			console.log(DeleteResultSet);
		}//返回删除后的结果
});
```
## 状态码
```
100：初始状态
101：鼠标拖动文件进入操作区域
102：已打开文件选择框
103：鼠标将文件移入但未松开或
104：鼠标将文件移出
105：已将文件放置到操作区域
106：文件选择框选择文件后关闭
107：文件选择框单击“取消”按钮已关闭
108：开始读取文件
109：文件读取中止或失败
110：文件读取数量超出限制
111：文件读取成功
112：开始查找（调用DeleteOPState函数返回）
113：删除成功（调用DeleteOPState函数返回）
114：删除失败（调用DeleteOPState函数返回）
115：开始上传文件
116：文件上传中止或失败
117：文件上传成功
```
## 返回示例

### 1.过程中返回
```
ResultObject{
    AttributeList:
        0:
          FileFormat: "image/png"
          FileName: "捕获.PNG"
          FileRoute: "捕获.PNG"
          FileSize: "34.3KB"
          PreviewFileFR: FileReader
                error: null
                onabort: null
                onerror: null
                onload: null
                onloadend: null
                onloadstart: null
                onprogress: null
                readyState: 2
                result: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABlc...
                [[Prototype]]: FileReader
          PreviewFileWin: "blob:http://127.0.0.1/1c8eb893-46c0-4fb0-aad8-7833f9a521d8"
          TimeDate: 1653576606526
          TormatTime: "2022/4/29 00:02:49"
          lastModified: 1651161769543
          [[Prototype]]: Object
        [[Prototype]]: Object
    ResultType: "部分结果"
    UploadList:
        0:
          File: File
            lastModified: 1651161769543
            lastModifiedDate: Fri Apr 29 2022 00:02:49 GMT+0800 (中国标准时间) {}
            name: "捕获.PNG"
            size: 35122
            type: "image/png"
            webkitRelativePath: ""
            [[Prototype]]: File
          FileName: "捕获.PNG"
          FileRoute: "捕获.PNG"
          SubmitTime: 1653576606526
        [[Prototype]]: Object
    [[Prototype]]: Object
[[Prototype]]: Object
}
```
### 2.全部结束后返回
```
ResultObject{
    AttributeList:
        0:
          FileFormat: "image/png"
          FileName: "捕获.PNG"
          FileRoute: "捕获.PNG"
          FileSize: "34.3KB"
          PreviewFileFR: FileReader {
              readyState: 2, 
              result: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABl...', 
              error: null, 
              onloadstart: null, 
              onprogress: null, 
            …}
          PreviewFileWin: "blob:http://127.0.0.1/1c8eb893-46c0-4fb0-aad8-7833f9a521d8"
          TimeDate: 1653576606526
          TormatTime: "2022/4/29 00:02:49"
          lastModified: 1651161769543
          [[Prototype]]: Object
    [[Prototype]]: Object
    ResultType: "全部结果"
    UploadList:
        0:
          File: File {
              name: '捕获.PNG', 
              lastModified: 1651161769543, 
              lastModifiedDate: Fri Apr 29 2022 00:02:49 GMT+0800 (中国标准时间), 
              webkitRelativePath: '', 
              size: 35122, …
            }
          FileName: "捕获.PNG"
          FileRoute: "捕获.PNG"
          SubmitTime: 1653576606526
          formData: FormData {}
          [[Prototype]]: Object
        [[Prototype]]: Object
    WholeSize: 35122
    WholeformData: FormData
    [[Prototype]]: FormData
[[Prototype]]: Object
}
```

## 说明
 1. DragElement和ClickElement都是非必须的，但必须有一个存在；同时，他们受OPPattern影响
 2. ReturnButtom也是非必须的；但如果它不存在，那么只能增加文件，不能删除文件
 3. QuantityLimit、ConsoleAutograph和OutputWarning都是非必须的；须注意OutputWarning，它可能在控制台输出很多警告
 4. FormatList非必须的；如果它存在，那么它必须是数组
 5. OPState、ResultSet和DeleteOPState、DeleteResultSet都是非必须的；如果它存在，那么它必须是函数（方法）；否则将有可能被默认参数替代或报错，亦或无法得到操作状态和结果
 6. 想要调用FileUpload.Delete函数（方法），必须先调用FileUpload函数（方法），否则会报错
 7. FileName、FileFormat、FileRoute必须存在，且格式必须为String
 8. SubmitTime必须存在，且格式必须为String或Number
 9. lastModified和FileSize至少存在一个，且必须为函数（方法）
 10. 返回的数据类型将是整数或对象
 11. 被调用的两个方法都只接受规定的属性，不接受外来属性
 12. 版本为v1.0.01，第一次试着写，欢迎指教