(function(event) {
	// 解决IE（对象不支持“assign”属性或方法）
	if (typeof Object.assign != 'function') {
		Object.assign = function(target) {
			'use strict';
			if (target == null) {
				throw new TypeError('Cannot convert undefined or null to object');
			}

			target = Object(target);
			for (var index = 1; index < arguments.length; index++) {
				var source = arguments[index];
				if (source != null) {
					for (var key in source) {
						if (Object.prototype.hasOwnProperty.call(source, key)) {
							target[key] = source[key];
						}
					}
				}
			}
			return target;
		};
	}
	// 解决IE（对象不支持“includes”属性或方法）
	if (!String.prototype.includes) {
		String.prototype.includes = function(search, start) {
			if (typeof start !== 'number') {
				start = 0;
			}

			if (start + search.length > this.length) {
				return false;
			} else {
				return this.indexOf(search, start) !== -1;
			}
		};
	}

	function getUrlState(parameter) {
		let Default = {
			type: 'get',
			url: null,
			RequestType: 'jsonp',
			async: true,
			CustomMethod:null,
			Timeout: 1000,
			State: function(state) {
				console.log(state);
			}
		}
		getUrlState.Default = Default;
		getUrlState.parameter = parameter;
		// parameter
		getUrlState.prototype.init(Object.assign({}, Default, parameter));
	}
	// 初始化
	getUrlState.prototype.init = function(parameter) {
		parameter.RequestType = parameter.RequestType.trim().toLowerCase() || 'jsonp';
		if (parameter.url) {
			if (parameter.url.replace(/(\s)/g, "")) {
				if (Object.prototype.toString.call(parameter.url) !== '[object String]') {
					getUrlState.prototype.error('url属性必须为String类型',parameter);
				}else{
					function UrlState(parameter) {
						this.state = null;
						this.data = null;
					}
					let UrlStateObject = new UrlState();
					getUrlState.UrlStateObject = UrlStateObject;
					getUrlState.DataFun = function(data) {
						UrlStateObject.data = data;
					}
				}
			} else {
				getUrlState.prototype.error('url属性必须存在',parameter);
			}
		} else {
			getUrlState.prototype.error('url属性必须存在',parameter);
		}

		parameter.Timeout = parseInt(parameter.Timeout);
		if (!parameter.Timeout || parameter.Timeout === NaN || parameter.Timeout === undefined) {
			parameter.Timeout = 1000;
		}

		if (parameter.Timeout < 0) {
			getUrlState.prototype.error('Timeout属性值不能小于零',parameter);
		}
		if (parameter.async && Object.prototype.toString.call(parameter.async) !== '[object Boolean]') {
			getUrlState.prototype.error('async属性必须为Boolean类型',parameter);
		}
		if (parameter.RequestType) {
			if (Object.prototype.toString.call(parameter.RequestType) === '[object String]') {
				if (!parameter.RequestType.replace(/(\s)/g, "")) {
					getUrlState.prototype.error('RequestType属性必须存在',parameter);
				}
			} else {
				getUrlState.prototype.error('RequestType属性必须为String类型',parameter);
			}
		} else {
			getUrlState.prototype.error('RequestType属性必须存在',parameter);
		}
		(function WebAgreement(url) {
			var check_www = 'w{3}' + '[^\\s]*';
			var check_http = '(https|http|ftp|rtsp|mms|tls|tcp|websocket|udp|dns)://' + '[^\\s]*';
			var strRegex = check_www + '|' + check_http;
			var httpReg = new RegExp(strRegex, 'gi');
			if (!url.match(httpReg)) {
				if (url.substring(0, 2) !== "./" && url.substring(0, 3) !== "../") {
					parameter.url = "./" + parameter.url;
				}
				getUrlState.prototype.Ajax(parameter);
			} else {
				parameter.url = url.match(httpReg)[0];
				if (parameter.RequestType.toLowerCase() === 'jsonp') {
					getUrlState.prototype.EstablishScript(parameter);
				} else {
					getUrlState.prototype.Ajax(parameter);
				}
			}
		})(parameter.url);
	}
	// Ajax方式判断
	getUrlState.prototype.Ajax = function(parameter) {
		var BxzAjax = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
		parameter.type = parameter.type.trim().toLowerCase() || 'get';
		if (parameter.async === 'false' || parameter.async === false) {
			parameter.async = false;
		} else {
			parameter.async = true;
		}

		BxzAjax.open(parameter.type, parameter.url, parameter.async);
		if (parameter.type === 'post') {
			BxzAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		}
		if (parameter.RequestType === 'json' && parameter.type === 'post') {
			BxzAjax.setRequestHeader("Content-Type", "application/json");
		}
		BxzAjax.send(null);
		var setTime = setTimeout(function() {
			// 取消请求
			BxzAjax.abort();
			getUrlState.prototype.load('Ajax', 404, parameter, null);
			clearTimeout(setTime);
		}, (parameter.Timeout) + 1);
		if (parameter.async) { //异步
			BxzAjax.onreadystatechange = function() {
				if (BxzAjax.readyState === 4) {
					if (BxzAjax.status === 200) {
						clearTimeout(setTime);
						if (BxzAjax.response) {
							// getUrlState.UrlStateObject.data = JSON.parse(BxzAjax.response);
						}
						getUrlState.prototype.load('Ajax', 200, parameter, null);
					}
				}
			}
		} else {
			if (BxzAjax.readyState === 4) {
				if (BxzAjax.status === 200) {
					clearTimeout(setTime);
					if (BxzAjax.response) {
						// getUrlState.UrlStateObject.data = JSON.parse(BxzAjax.response);
					}
					getUrlState.prototype.load('Ajax', 200, parameter, null);
				}
			}
		}
	}

	// 创建script标签
	getUrlState.prototype.EstablishScript = function(parameter) {
		try {
			var symbol = parameter.url.includes("?") ? "&" : "?";
			//创建script标签
			var script = document.createElement("script");
			script.type = 'text/javascript';
			script.charset = 'scriptCharset';
			script.async='async';
			//填写请求地址,添加要传递的数据,传递一个回调函数
			script.src = parameter.url + symbol + "&cb=getUrlState.DataFun";
			//这个时候我们需要把准备好的script标签添加到页面 最好是添加到脚本以后
			document.body.appendChild(script);
			getUrlState.prototype.EstablishIframe(parameter);
			getUrlState.prototype.ScriptState(script, parameter);
		} catch (error) {
			getUrlState.prototype.error('创建script标签时发生错误',parameter);
		} //捕获错误，其中error就是new Error() 的实例
	}
	// script标签状态
	// label标签
	getUrlState.prototype.ScriptState = function(script, parameter) {
		try {
			var Time = 0;
			var setTime = setTimeout(function() {
				Time = (parameter.Timeout) + 1;
				clearTimeout(setTime);
			}, (parameter.Timeout) + 1);
			if (script.readyState && Time <= parameter.Timeout) { //IE
				script.onreadystatechange = function() {
					if (script.readyState == 'complete' || script.readyState == 'loaded') {
						script.onreadystatechange = null;
						getUrlState.prototype.load(null, 200, parameter, script);
					} else {
						getUrlState.prototype.load(null, 404, parameter, script);
					}
				}
			} else if (Time <= parameter.Timeout) { //非IE
				script.onload = function() {
					getUrlState.prototype.load(null, 200, parameter, script);
				}
				script.onerror = function(error) {
					getUrlState.prototype.load(null, 404, parameter, script);
				}
			} else {
				getUrlState.prototype.load(null, 404, parameter, script);
			}
		} catch (error) {
			getUrlState.prototype.error('获取script标签状态时出现错误',parameter);
		} //捕获错误，其中error就是new Error() 的实例
	}
	
	
	getUrlState.prototype.EstablishIframe = function(parameter) {
		try {
			var iframe = document.createElement("iframe");
			iframe.width = 0;
			iframe.height = 0;
			iframe.style.display = 'none';
			iframe.frameborder = 'on';
			iframe.src = parameter.url;
			document.body.appendChild(iframe);
	    } catch (error) {
			getUrlState.prototype.error('获取Iframe标签状态时出现错误',parameter);
		} //捕获错误，其中error就是new Error() 的实例
	}
	
	// <iframe src="" width="" height=""></iframe>
	
	
	
	
	// 完成
	getUrlState.prototype.load = function(who, State, parameter, script) {
		if (parameter.CustomMethod && Object.prototype.toString.call(parameter.CustomMethod) === '[object Function]') {
			getUrlState.UrlStateObject['state'] = State;
			parameter.CustomMethod(getUrlState.UrlStateObject);
		}else{
			if (parameter.State && Object.prototype.toString.call(parameter.State) === '[object Function]') {
				getUrlState.UrlStateObject['state'] = State;
				console.log(getUrlState.UrlStateObject)
				parameter.State(getUrlState.UrlStateObject);
			}
		}
		if (who !== 'Ajax' && who !== 'error') {
			document.body.removeChild(script);
		}
	}
	// 代码或操作出错
	getUrlState.prototype.error = function(error,parameter) {
		getUrlState.prototype.load('error', 404, parameter, null);
		throw Error(error);
	}
	event.getUrlState = getUrlState;
})(window);