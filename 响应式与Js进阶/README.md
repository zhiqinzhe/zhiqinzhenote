# 响应式页面与Js进阶

### 特别说明
```
本文件夹所有文件和代码均来自bilibili平台id名为“大碗绿豆沙”的up主
```

### 介绍
本项目包含了 HTML、CSS、JavaScript相关知识和响应式页面、Js进阶小实例

### 目录
1. [AdminDashboard](./AdminDashboard/index.html)
2. [article-preview-component-master](./article-preview-component-master/index.html)
3. [css产品卡片设计](./css产品卡片设计/index.html)
4. [css动态登录页面](./css动态登录页面/index.html)
5. [QrCode](./QrCode/index.html)
6. [RidexMaster](./RidexMaster/index.html)
7. [个性化音乐播放器](./个性化音乐播放器/index.html)
8. [管理面板](./管理面板/index.html)
9. [获取用户位置](./获取用户位置/index.html)
10. [识别用户作系统](./识别用户作系统/index.html)
11. [响应式波浪登录页面](./响应式波浪登录页面/index.html)
12. [用视频做文字背景](./用视频做文字背景/index.html)
13. [昼夜模式管理菜单](./昼夜模式管理菜单/index.html)
14. [昼夜模式日历](./昼夜模式日历/index.html)

### 请注意
1. 部分代码为本人依据视频内容抄写下来的
2. 本人对运行出错或运行效果不理想的代码有细微修改
3. 若代码出错或运行效果不理想，敬请谅解和反馈