const wrapper=document.querySelector('.wrapper'),
form = wrapper.querySelector('form'),
fileInp = form.querySelector('input'),
infoText=form.querySelector('p'),
closeBtn=wrapper.querySelector('.close'),
copyBtn=wrapper.querySelector('.copy');
function fetchRequest(formData,file){
	infoText.innerText="Scanning QR Code...";
	fetch("http://api.qrserver.com/v1/read-qr-code/",{
		method:"POST",
		body:formData
	}).then(res => res.json()).then(result => {
		result = result[0].symbol[0].data;
		infoText.innerText=result?"Upload QR Code to Read":"Couldn't Scan QR Code";
		if(!result) return;
		wrapper.querySelector('textarea').innerText = result;
		form.querySelector('img').src = URL.createObjectURL(file);
		wrapper.classList.add("active");
	}).catch(()=>{
		infoText.innerText = "Couldn't Scan QR Code";
	});
}

form.addEventListener('change',e => {
	let file = e.target.files[0];
	if(!file) return;
	let formData=new FormData();
	formData.append("file",file);
	fetchRequest(formData,file);
});


copyBtn.addEventListener('click',() => {
	let text=wrapper.querySelector('textarea').textContent;
	navigator.clipboard.writeText(text);
});

form.addEventListener('click',() => fileInp.click());
closeBtn.addEventListener('click',() => {
	setTimeout(() => {
		wrapper.classList.remove("active")
		}, 500);
	});



// 获取所有按钮对象
        const btns=document.querySelectorAll("button");
		console.log(btns)
        // 循环所有按钮,并为每一个按钮添加点击事件
        btns.forEach(btn=>{
            btn.addEventListener("click",e=>{
                // 创建span元素,并设置其位置为鼠标点击的位置
                let span=document.createElement("span");
                span.style.left=e.offsetX+"px";
                span.style.top=e.offsetY+"px";
                // 将span元素添加到按钮标签里
                btn.appendChild(span);
                // 1秒后删除span元素
                setTimeout(() => {
                    span.remove();
                }, 1000);
            })
        })