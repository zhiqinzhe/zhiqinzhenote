# 隐私政策

### Privacy policy

这个插件没有任何隐私政策，因为它没有采集你的任何个人信息；如果你打开它的“详细信息”，你会发现“权限”一栏如下图一样显示为“此扩展不需要特殊权限”。

This plugin does not have any privacy policy as it does not collect any personal information from you; If you open its "Details", you will find that the "Permissions" column displays as "This extension does not require special permissions" as shown in the following image.

![ColorToolAuthority](https://gitee.com/zhiqinzhe/Picture/raw/master/ImgPath/ColorToolAuthority.png)

这个插件只需要基础的权限，因为它只具备拾取页面颜色的能力。 这个插件不需要特殊的权限，因为它不会收集任何你的个人信息。

This plugin only requires basic permissions because it only has the ability to pick up page colors. This plugin does not require special permissions as it will not collect any personal information about you.
		