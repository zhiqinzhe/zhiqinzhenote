(function () {
	const colorData = localStorage.getItem("ColorToolStorage") || "#255696";
	ColorTool(colorData).then(function (ColorData) {
		// console.log(ColorData) // 颜色数据
	});

	const setColorTool = document.querySelector(".setColorTool"),
	SettingsButton = document.querySelector(".SettingsButton"),
		ContainerPanelContainer = document.querySelector(".ContainerPanelContainer"),
		ContainerPanel = document.querySelector(".ContainerPanel"),
		appear = document.querySelector("#appear"),
		InputBoxOne = document.querySelector('.InputBoxOne'),
		InputBoxTwo = document.querySelector('.InputBoxTwo'),
		InputBoxThree = document.querySelector('.InputBoxThree'),
		LeftAndRight = document.querySelector('.LeftAndRight');


	function openCenteredWindow(url, openMode, width, height, replace) {
		// 获取屏幕宽度和高度
		const screenWidth = window.screen.width;
		const screenHeight = window.screen.height;

		// 计算新窗口的位置
		const left = (screenWidth - width) / 2;
		const top = (screenHeight - height) / 2;

		try {
			// 尝试打开窗口
			window.open(url, openMode, `width=${width},height=${height},left=${left},top=${top}`, replace);
		} catch (error) {
			// 捕获异常并处理
			window.open(url, replace);
		}
	}

	setColorTool.addEventListener("click", () => {
		// 使用函数打开一个居中的窗口，例如宽度为 600px，高度为 400px
		ContainerPanelContainer.style.display = "flex";
	})

	SettingsButton.addEventListener("click", () => {
		// 使用函数打开一个居中的窗口，例如宽度为 600px，高度为 400px
		openCenteredWindow('./pages/options.html', '_blank', 1256, 720, false);
	})

	if (chrome.storage) {
		chrome.storage.sync.get(['appearChecked'], function (result) {
			if (result.appearChecked === true || result.appearChecked === false) {
				appear.checked = result.appearChecked
			}
		});

		chrome.storage.sync.get(['wholeChecked'], function (result) {
			if (result.wholeChecked === true) {
				LeftAndRight.children[0].style.display = "block";
				LeftAndRight.children[1].style.display = "block";
				InputBoxOne.style.display = "block";
				InputBoxTwo.style.display = "none";
				InputBoxThree.style.display = "none";
			} else {
				chrome.storage.sync.get(['HEXEleChecked'], function (result) {
					if (result.HEXEleChecked === true) {
						LeftAndRight.children[0].style.display = "none";
						LeftAndRight.children[1].style.display = "none";
						InputBoxTwo.style.display = "none";
						InputBoxThree.style.display = "none";
						InputBoxOne.style.display = "block";
					}
				});

				chrome.storage.sync.get(['RGBEleChecked'], function (result) {
					if (result.RGBEleChecked === true) {
						LeftAndRight.children[0].style.display = "none";
						LeftAndRight.children[1].style.display = "none";
						InputBoxOne.style.display = "none";
						InputBoxThree.style.display = "none";
						InputBoxTwo.style.display = "flex";
					}
				});

				chrome.storage.sync.get(['HSLEleChecked'], function (result) {
					if (result.HSLEleChecked === true) {
						LeftAndRight.children[0].style.display = "none";
						LeftAndRight.children[1].style.display = "none";
						InputBoxOne.style.display = "none";
						InputBoxTwo.style.display = "none";
						InputBoxThree.style.display = "flex";
					}
				});
			}
		});

	}

	ContainerPanelContainer.addEventListener("click", () => {
		ContainerPanelContainer.classList.add("EndContainerPanelContainer");
		ContainerPanel.classList.add("EndContainerPanel");
		let setTime = setTimeout(function () {
			ContainerPanelContainer.style.display = "none";
			ContainerPanelContainer.classList.remove("EndContainerPanelContainer");
			ContainerPanel.classList.remove("EndContainerPanel");
			clearTimeout(setTime);
		}, 300);
	})

	ContainerPanel.addEventListener("click", function (e) {
		if (e && e.stopPropagation) {
			e.stopPropagation();
		}
	})

	appear.addEventListener("change", () => {
		if (chrome.storage) {
			chrome.storage.sync.set({ appearChecked: appear.checked });
		}
	})

})(window);