// 用于存储已创建的菜单项ID的集合  
const createdMenuItems = new Set();

// 创建一个新的上下文菜单项  
function createContextMenuItem(id, title, contexts) {

	if (!createdMenuItems.has(id)) {
		chrome.contextMenus.create({
			"id": id,
			"title": title,
			"contexts": contexts
		}, function () {
			if (chrome.runtime.lastError) {
				console.log(
					'%c%s',
					'border: 1px solid white;border-radius: 5px;padding: 2px 5px;color: white;font-weight: bold;background-color: #ab5a5a;',
					'创建右键菜单项时出错。\nError creating context menu item.');
			} else {
				createdMenuItems.add(id);
			}
		});
	}
}

// 使用函数来创建菜单项  
createContextMenuItem("ColorToolGetColor", "获取颜色", ["page"]);

function openColorPanel() {
	if (!document.querySelector('#openColorPanel')) {
		let isAllowed = confirm('是否允许取色面板出现在页面上？');
		if (isAllowed) {
			chrome.storage.sync.set({ appearChecked: true });
			let setTime = setTimeout(function () {
				openColorPanel();
				clearTimeout(setTime);
			}, 24);
		}
	} else {
		document.querySelector('#openColorPanel').click();

		let setTime = setTimeout(function () {
			let setTimeClick = setTimeout(function () {
				if (document.querySelector('#getColor')) {
					document.querySelector('#getColor').click();
				}
				clearTimeout(setTimeClick);
			}, 500);
			clearTimeout(setTime);
		}, 12);
	}
}

chrome.contextMenus.onClicked.addListener(function (clickData) {

	if (clickData.menuItemId === "ColorToolGetColor") {
		chrome.tabs.query({
			currentWindow: true,
			active: true
		}, function (tabs) {
			if (tabs[0]) {
				let currentPageUrl = tabs[0].url;
				if (currentPageUrl.startsWith('chrome-extension://') === false && currentPageUrl.startsWith('edge://') === false) {
					chrome.scripting.executeScript({
						target: { tabId: tabs[0].id },
						function: openColorPanel
					});
				}
			} else {
				console.log(
					'%c%s',
					'border: 1px solid white;border-radius: 5px;padding: 2px 5px;color: white;font-weight: bold;background-color: #ab5a5a;',
					'发生未知错误！\nUnknown error occurred!');
			}
		});
	}
});

chrome.runtime.onInstalled.addListener(() => {
	console.log(
		'%c%s',
		'border: 1px solid white;border-radius: 5px;padding: 2px 3px;color: white;font-weight: bold;background-color: #255696;',
		'你已经成功安装并使用ColorTool插件。\nYou have successfully installed and used the ColorTool plug-in.');
	console.log(
		'%c%s',
		'border: 1px solid white;border-radius: 5px;padding: 2px 5px;color: white;font-weight: bold;background-color: #ad4748;',
		'开发人员：知勤者\n个人主页：blog.csdn.net/zhiqinzhe?type=blog');
});