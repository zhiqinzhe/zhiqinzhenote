const SettingsMasking = document.querySelector(".SettingsMasking"),
    SettingsContainer = document.querySelector(".SettingsContainer"),
    ColorData = document.querySelector("#ColorData"),
    Settings = document.querySelector("#Settings"),
    EndSettings = document.querySelector("#EndSettings"),
    SettingsBox = document.querySelector(".SettingsBox"),
    appear = document.querySelector("#appear"),
    whole = document.querySelector("#whole"),
    HEXEle = document.querySelector("#HEX"),
    RGBEle = document.querySelector("#RGB"),
    HSLEle = document.querySelector("#HSL"),
    HEXlabel = document.querySelector("#HEXlabel"),
    RGBlabel = document.querySelector("#RGBlabel"),
    HSLlabel = document.querySelector("#HSLlabel");

ColorData.addEventListener("click", function () {
    Settings.classList.remove("SettingsOptionBlock");
    this.classList.add("SettingsOptionBlock");
    if (SettingsBox.children[0]) {
        SettingsBox.children[1].style.display = "none";
        SettingsBox.children[0].style.display = "block";
    }
})

Settings.addEventListener("click", function () {
    if (SettingsBox.children[1]) {
        ColorData.classList.remove("SettingsOptionBlock");
        this.classList.add("SettingsOptionBlock");
        SettingsBox.children[0].style.display = "none";
        SettingsBox.children[1].style.display = "block";
    }
})

EndSettings.addEventListener("click", function () {
    SettingsMasking.classList.add("EndSettingsMasking");
    SettingsContainer.classList.add("EndSettingsContainer");
    let setTime = setTimeout(function () {
        SettingsMasking.style.display = "none";
        SettingsMasking.classList.remove("EndSettingsMasking");
        SettingsContainer.classList.remove("EndSettingsContainer");
        clearTimeout(setTime);
    }, 300);
})

// 假设的初始数据  
let ColorDataList = {
    "logoColor": { idAndfor: "logoColor", colorName: "默认的颜色", colorValue: '#255696' },
    "red": { idAndfor: "red", colorName: "红色", colorValue: '#FF0000' },
    "orange": { idAndfor: "orange", colorName: "橙色", colorValue: '#FFA500' },
    "yellow": { idAndfor: "yellow", colorName: "黄色", colorValue: '#FFFF00' },
    "green": { idAndfor: "green", colorName: "绿色", colorValue: '#00FF00' },
    "blue": { idAndfor: "blue", colorName: "蓝色", colorValue: '#0000FF' },
    "indigo": { idAndfor: "indigo", colorName: "靛色", colorValue: '#4B0082' },
    "purple": { idAndfor: "purple", colorName: "紫色", colorValue: '#800080' }
};

let ColorList = {}; // 用于存储哪些复选框被选中的状态  
Object.keys(ColorDataList).forEach(key => {
    ColorList[ColorDataList[key].idAndfor] = { checked: false }; // 初始化为未选中  
});

if (chrome.storage) {
    chrome.storage.sync.get(['appearRadio'], function (result) {
        if (result.appearRadio) {
            ColorList[result.appearRadio] = { checked: true };
        } else {
            ColorList["logoColor"] = { checked: true };
        }
        renderColorList();
    });
} else {
    ColorList["logoColor"] = { checked: true };
    renderColorList();
}

// 渲染列表  
function renderColorList() {
    let ul = document.createElement('ul');
    ul.className = 'ColorList';

    Object.keys(ColorDataList).forEach(key => {
        let value = ColorDataList[key];
        let li = document.createElement('li');
        li.setAttribute('data-key', key); // 使用 data- 属性代替 :key  

        // 创建复选框  
        let radio = document.createElement('input');
        radio.type = 'radio';
        radio.name = "radioColorValue";
        radio.id = value.idAndfor;
        radio.value = value.colorValue;
        radio.checked = ColorList[value.idAndfor].checked; // 设置初始选中状态  
        radio.addEventListener('change', function () {
            ColorList[value.idAndfor].checked = this.checked; // 更新状态  
            if (chrome.storage) {
                chrome.storage.sync.set({ appearRadio: value.idAndfor });
            }
            localStorage.setItem("ColorToolStorage", value.colorValue);
        });

        li.appendChild(radio);

        let CamouflageLiDiv = document.createElement('div');
        CamouflageLiDiv.className = "CamouflageLi";
        let LabelAndTitleContainerDiv = document.createElement('div');
        LabelAndTitleContainerDiv.className = "LabelAndTitleContainer";
        let label = document.createElement('label');
        label.setAttribute('for', value.idAndfor);
        let i = document.createElement('i');
        i.className = "icon-custom-selected";
        let span = document.createElement('span');
        span.innerText = value.colorValue;
        label.appendChild(i);
        LabelAndTitleContainerDiv.appendChild(label);
        LabelAndTitleContainerDiv.appendChild(span);
        CamouflageLiDiv.appendChild(LabelAndTitleContainerDiv);
        let InformationContainerDiv = document.createElement('div');
        InformationContainerDiv.className = "InformationContainer";
        let ColorContainer = document.createElement('div');
        ColorContainer.className = "ColorContainer";
        ColorContainer.style.backgroundColor = value.colorValue;
        let EngineInformationDiv = document.createElement('div');
        EngineInformationDiv.className = "EngineInformation";
        let title = document.createElement('h3');
        title.innerText = value.colorName;
        EngineInformationDiv.appendChild(title);
        InformationContainerDiv.appendChild(ColorContainer);
        InformationContainerDiv.appendChild(EngineInformationDiv);
        CamouflageLiDiv.appendChild(InformationContainerDiv);
        li.appendChild(CamouflageLiDiv)
        ul.appendChild(li);
    });

    // 假设你有一个元素用于放置这个列表  
    let container = document.querySelector('.SettingsBox');
    container.insertBefore(ul, container.firstChild);
}

function wholeCheckedFalse() {
    HEXlabel.classList.remove("DisableClass");
    RGBlabel.classList.remove("DisableClass");
    HSLlabel.classList.remove("DisableClass");

    HEXEle.disabled = false;
    RGBEle.disabled = false;
    HSLEle.disabled = false;
}

if (chrome.storage) {
    chrome.storage.sync.get(['appearChecked'], function (result) {
        if (result.appearChecked === true || result.appearChecked === false) {
            appear.checked = result.appearChecked
        }
    });

    chrome.storage.sync.get(['wholeChecked'], function (result) {
        if (result.wholeChecked === true || result.wholeChecked === false) {
            whole.checked = result.wholeChecked
        }

        if (result.wholeChecked === false) {
            wholeCheckedFalse();
        }
    });

    chrome.storage.sync.get(['HEXEleChecked'], function (result) {
        if (result.HEXEleChecked === true || result.HEXEleChecked === false) {
            HEXEle.checked = result.HEXEleChecked
        }
    });

    chrome.storage.sync.get(['RGBEleChecked'], function (result) {
        if (result.RGBEleChecked === true || result.RGBEleChecked === false) {
            RGBEle.checked = result.RGBEleChecked
        }
    });

    chrome.storage.sync.get(['HSLEleChecked'], function (result) {
        if (result.HSLEleChecked === true || result.HSLEleChecked === false) {
            HSLEle.checked = result.HSLEleChecked
        }
    });
}

appear.addEventListener("change", () => {
    if (chrome.storage) {
        chrome.storage.sync.set({ appearChecked: appear.checked });
    }
})

whole.addEventListener("change", () => {

    if (whole.checked) {
        HEXlabel.classList.add("DisableClass");
        RGBlabel.classList.add("DisableClass");
        HSLlabel.classList.add("DisableClass");

        HEXEle.disabled = true;
        RGBEle.disabled = true;
        HSLEle.disabled = true
    } else {
        wholeCheckedFalse();
    }
    if (chrome.storage) {
        chrome.storage.sync.set({ wholeChecked: whole.checked });
    }
})

HEXEle.addEventListener("change", () => {
    if (chrome.storage) {
        whole.checked = false;
        chrome.storage.sync.set({ wholeChecked: false });
        chrome.storage.sync.set({ RGBEleChecked: false });
        chrome.storage.sync.set({ HSLEleChecked: false });
        chrome.storage.sync.set({ HEXEleChecked: HEXEle.checked });
    }
})

RGBEle.addEventListener("change", () => {
    if (chrome.storage) {
        whole.checked = false;
        chrome.storage.sync.set({ wholeChecked: false });
        chrome.storage.sync.set({ HEXEleChecked: false });
        chrome.storage.sync.set({ HSLEleChecked: false });
        chrome.storage.sync.set({ RGBEleChecked: RGBEle.checked });
    }
})

HSLEle.addEventListener("change", () => {
    if (chrome.storage) {
        whole.checked = false;
        chrome.storage.sync.set({ wholeChecked: false });
        chrome.storage.sync.set({ HEXEleChecked: false });
        chrome.storage.sync.set({ RGBEleChecked: false });
        chrome.storage.sync.set({ HSLEleChecked: HSLEle.checked });
    }
})   