(function (event) {
	event = event || document;
	let rgba = {
		r: 0,
		g: 0,
		b: 0,
		a: 1
	},
		hsb = {
			h: 0,
			s: 100,
			b: 100
		};

	let Initial = null,
		setTime = null,
		throttle = null;

	function ColorTool(str) {
		ColorTool.prototype.Ele = {
			sample: document.querySelector('.sample'),
			ColorToolContainer: document.querySelector('.ColorToolContainer'),
			PanelContainer: document.querySelector('.PanelContainer'),
			panelCanvas: document.querySelector('#panelCanvas'),
			DotOne: document.querySelector('.DotOne'),
			Discoloration: document.querySelector('.Discoloration'),
			DotTwo: document.querySelector('.DotTwo'),
			input: document.querySelectorAll('input'),
			ControlButton: document.querySelector('.control>button'),
			LeftAndRightlSpan: document.querySelectorAll('.LeftAndRight>span'),
			InputBoxOne: document.querySelector('.InputBoxOne'),
			InputBoxTwo: document.querySelector('.InputBoxTwo'),
			InputBoxThree: document.querySelector('.InputBoxThree'),
			getColor: document.querySelector(".getColor"),
			constant: 0
		};
		Initial = str;
		return new ColorTool.prototype.init(str);
	}
	
	ColorTool.prototype.init = function (str) {
		ColorTool.prototype.initANDsetColor(str);
		ColorTool.prototype.getColorFun();
		return this;
	}

	ColorTool.prototype.init.prototype = ColorTool.prototype;

	ColorTool.prototype.then = function (parameter) {
		ColorTool.prototype.then.prototype.thenFun = parameter;
	}

	ColorTool.setColor = function (str) {
		if (ColorTool.prototype.initANDsetColor) {
			ColorTool.prototype.initANDsetColor(str);
		} else {
			throw new Error('未调用ColorTool函数');
		}
	}

	ColorTool.prototype.getColorFun = function () {
		if ('EyeDropper' in window) { // 判断是否支持这个api
			const eyeDropper = new EyeDropper(); // 创建对象

			ColorTool.prototype.Ele.getColor.addEventListener('click', async () => {
				try {
					const ColorResult = await eyeDropper.open();// 取得吸取结果
					ColorTool.setColor(ColorResult.sRGBHex);// 取得颜色并设置颜色
				} catch (e) {
					console.log(
						'%c%s',
						'border: 1px solid white;border-radius: 5px;padding: 2px 5px;color: white;font-weight: bold;background-color: #ab5a5a;',
						'发生了一些意外！\nSomething unexpected happened!');
				}
			});
		}
	}

	ColorTool.prototype.initANDsetColor = function (str) {
		if (str.match(/rgb\(|\Rgb\(/g) && str.match(/\)|\);/g) && str.match(/,/g)) {
			if (str.match(/,/g).length === 2) {
				str = str.replace(/rgb\(|\Rgb\(/g, '').replace(/\)|\);/g, '');
				let strArr = str.split(',');
				ColorTool.prototype.Ele.input[1].value = parseInt(strArr[0]);
				ColorTool.prototype.Ele.input[2].value = parseInt(strArr[1]);
				ColorTool.prototype.Ele.input[3].value = parseInt(strArr[2]);
				ColorTool.prototype.RgbInput();
			}
		} else {
			ColorTool.prototype.Ele.input[0].value = str;
			ColorTool.prototype.HexInput();
		}
		ColorTool.prototype.EventCollection();
	}


	ColorTool.prototype.hsbToRgb = function (hsb) {
		var rgb = {};
		var h = hsb.h;
		var s = hsb.s * 255 / 100;
		var v = hsb.b * 255 / 100;

		if (s == 0) {
			rgb.r = rgb.g = rgb.b = v;
		} else {
			var t1 = v;
			var t2 = (255 - s) * v / 255;
			var t3 = (t1 - t2) * (h % 60) / 60;
			if (h === 360) h = 0;
			if (h < 60) {
				rgb.r = t1;
				rgb.b = t2;
				rgb.g = t2 + t3
			} else if (h < 120) {
				rgb.g = t1;
				rgb.b = t2;
				rgb.r = t1 - t3
			} else if (h < 180) {
				rgb.g = t1;
				rgb.r = t2;
				rgb.b = t2 + t3
			} else if (h < 240) {
				rgb.b = t1;
				rgb.r = t2;
				rgb.g = t1 - t3
			} else if (h < 300) {
				rgb.b = t1;
				rgb.g = t2;
				rgb.r = t2 + t3
			} else if (h < 360) {
				rgb.r = t1;
				rgb.g = t2;
				rgb.b = t1 - t3
			} else {
				rgb.r = 0;
				rgb.g = 0;
				rgb.b = 0
			}
		}

		return {
			r: Math.round(rgb.r),
			g: Math.round(rgb.g),
			b: Math.round(rgb.b)
		};
	}

	ColorTool.prototype.rgbToHex = function (rgb) {
		var hex = [
			rgb.r.toString(16),
			rgb.g.toString(16),
			rgb.b.toString(16)
		];
		hex.map(function (str, i) {
			if (str.length == 1) {
				hex[i] = '0' + str;
			}
		});
		return hex.join('');
	}

	ColorTool.prototype.hexToRgb = function (hex) {
		var hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
		return {
			r: hex >> 16,
			g: (hex & 0x00FF00) >> 8,
			b: (hex & 0x0000FF)
		};
	}

	ColorTool.prototype.hexToHsb = function (hex) {
		return this.rgbToHsb(this.hexToRgb(hex));
	}

	ColorTool.prototype.rgbToHsb = function (rgb) {
		var hsb = {
			h: 0,
			s: 0,
			b: 0
		};
		var min = Math.min(rgb.r, rgb.g, rgb.b);
		var max = Math.max(rgb.r, rgb.g, rgb.b);
		var delta = max - min;
		hsb.b = max;
		hsb.s = max != 0 ? 255 * delta / max : 0;
		if (hsb.s != 0) {
			if (rgb.r == max) hsb.h = (rgb.g - rgb.b) / delta;
			else if (rgb.g == max) hsb.h = 2 + (rgb.b - rgb.r) / delta;
			else hsb.h = 4 + (rgb.r - rgb.g) / delta;
		} else hsb.h = -1;
		hsb.h *= 60;
		if (hsb.h < 0) hsb.h += 360;
		hsb.s *= 100 / 255;
		hsb.b *= 100 / 255;
		return hsb;
	}

	ColorTool.prototype.rgbToHsl = function (rgb) {
		var HslArray = {
			h: rgb.r / 255,
			s: rgb.g / 255,
			l: rgb.b / 255
		};

		var max = Math.max(HslArray.h, HslArray.s, HslArray.l);
		var min = Math.min(HslArray.h, HslArray.s, HslArray.l);
		var difference = max - min;


		var hsl = { h: 0, s: 0, l: 0 };

		if (max == min) {
			hsl.h = 0;
		} else if (max == HslArray.h && HslArray.s >= HslArray.l) {
			hsl.h = 60 * (HslArray.s - HslArray.l) / difference;
		} else if (max == HslArray.h && HslArray.s < HslArray.l) {
			hsl.h = 60 * (HslArray.s - HslArray.l) / difference + 360;
		} else if (max == HslArray.s) {
			hsl.h = 60 * (HslArray.l - HslArray.h) / difference + 120;
		} else if (max == HslArray.l) {
			hsl.h = 60 * (HslArray.h - HslArray.s) / difference + 240;
		}

		var sum = max + min;


		hsl.l = sum / 2;

		if (hsl.l == 0 || max == min) {
			hsl.s = 0;
		} else if (0 < hsl.l && hsl.l <= 0.5) {
			hsl.s = difference / sum;
		} else {
			hsl.s = difference / (2 - sum);
		}
		hsl.h = Math.round(hsl.h);
		return hsl;
	}

	ColorTool.prototype.hslToRgb = function (H = 0, S = 0, L = 0) {
		S /= 100;
		L /= 100;

		let CXM = {
			C: (1 - Math.abs(2 * L - 1)) * S,
			X: ((1 - Math.abs(2 * L - 1)) * S) * (1 - Math.abs(((H / 60) % 2) - 1)),
			M: L - ((1 - Math.abs(2 * L - 1)) * S) / 2
		}

		let vRGB = []
		if (H >= 0 && H < 60) {
			vRGB.push(CXM.C, CXM.X, 0)
		} else if (H >= 60 && H < 120) {
			vRGB.push(CXM.X, CXM.C, 0)
		} else if (H >= 120 && H < 180) {
			vRGB.push(0, CXM.C, CXM.X)
		} else if (H >= 180 && H < 240) {
			vRGB.push(0, CXM.X, CXM.C)
		} else if (H >= 240 && H < 300) {
			vRGB.push(CXM.X, 0, CXM.C)
		} else if (H >= 300 && H < 360) {
			vRGB.push(CXM.C, 0, CXM.X)
		}
		const [vR, vG, vB] = vRGB
		let RGB = {
			r: Math.round(255 * (vR + CXM.M)),
			g: Math.round(255 * (vG + CXM.M)),
			b: Math.round(255 * (vB + CXM.M))
		}
		return RGB
	}

	ColorTool.prototype.setValue = function (rgb) {
		ColorTool.prototype.Ele.input[0].value = "#" + this.rgbToHex(rgb);
		ColorTool.prototype.Ele.input[1].value = rgb.r;
		ColorTool.prototype.Ele.input[2].value = rgb.g;
		ColorTool.prototype.Ele.input[3].value = rgb.b;
		let hsl = ColorTool.prototype.rgbToHsl(rgb);
		ColorTool.prototype.Ele.input[4].value = hsl.h;
		ColorTool.prototype.Ele.input[5].value = Math.round(hsl.s * 100) + "%";
		ColorTool.prototype.Ele.input[6].value = Math.round(hsl.l * 100) + "%";
	}

	ColorTool.prototype.changeColor = function () {
		let rgb = this.hsbToRgb(hsb);
		this.setValue(rgb);
		rgba.r = rgb.r;
		rgba.g = rgb.g;
		rgba.b = rgb.b;
		localStorage.setItem("ColorToolStorage", "#" + this.rgbToHex(rgb));
		ColorTool.prototype.Ele.sample.style.backgroundColor = 'rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b +
			',' + rgba.a + ')';
		if (!(rgb.r >= 196 && rgb.g >= 196 && rgb.b >= 196)) {
			document.documentElement.style.setProperty('--MyColor', 'rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ',' + rgba.a + ')');
		}
		clearTimeout(throttle);
		throttle = setTimeout(function () {
			if (ColorTool.prototype.thenFunExistence('rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')')) {
				ColorTool.prototype.then.prototype.thenFun('rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')');
			}
			clearTimeout(throttle);
		}, 36);
	}

	ColorTool.prototype.thenFunExistence = function (ColorData) {
		if (ColorTool.prototype.then.prototype.thenFun) {
			return true;
		} else {
			clearTimeout(setTime);
			setTime = setTimeout(function () { //延时防止调用thenFun时发生越界
				if (ColorTool.prototype.thenFunExistence(ColorData)) {
					ColorTool.prototype.then.prototype.thenFun(ColorData);
				}
				clearTimeout(setTime);
			}, 5);
		}
		return false;
	}

	ColorTool.prototype.PanelCalculation = function (x, y) {
		let MaxLeft = Math.max(0, Math.min(x, 216));
		let MaxTop = Math.max(0, Math.min(y, 124));
		hsb.s = 100 * MaxLeft / 216;
		hsb.b = 100 * (124 - MaxTop) / 124;
		this.changeColor();
	}

	ColorTool.prototype.PanelColor = function (ColorData) {
		let canvas = ColorTool.prototype.Ele.panelCanvas;
		if (canvas && canvas.getContext) {
			let ctx = canvas.getContext("2d");
			// 底色填充，也就是（举例红色）到白色
			let gradientBase = ctx.createLinearGradient(3, 0, 216, 0);
			gradientBase.addColorStop(1, ColorData);
			gradientBase.addColorStop(0, "rgba(255,255,255,1)");
			ctx.fillStyle = gradientBase;
			ctx.fillRect(0, 0, 216, 124);
			// 第二次填充，黑色到透明
			let gradientScreen = ctx.createLinearGradient(0, 3, 0, 124);
			gradientScreen.addColorStop(0, "transparent");
			gradientScreen.addColorStop(1, "rgba(0,0,0,1)");
			ctx.fillStyle = gradientScreen;
			ctx.fillRect(0, 0, 216, 124);
		}
	}

	ColorTool.prototype.BarCalculation = function (x) {
		let Left = Math.max(0, Math.min(x, 216));
		hsb.h = 360 * Left / 216;
		let rgb = this.hsbToRgb({
			h: hsb.h,
			s: 100,
			b: 100
		});
		ColorTool.prototype.PanelColor('rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ',' + rgba.a + ')');
		this.changeColor();
		// rgba.a = X / elem_width;//调透明度的
	}



	ColorTool.prototype.panel = function (event) {
		ColorTool.prototype.Ele.DotOne.style.top = (event.offsetY - (ColorTool.prototype.Ele.DotOne.offsetHeight / 2)) + "px";
		ColorTool.prototype.Ele.DotOne.style.left = (event.offsetX - (ColorTool.prototype.Ele.DotOne.offsetWidth / 2)) + "px";
		ColorTool.prototype.PanelCalculation(event.offsetX, event.offsetY);
	}

	ColorTool.prototype.DiscolorationFun = function (event) {
		ColorTool.prototype.Ele.DotTwo.style.left = (event.offsetX - (ColorTool.prototype.Ele.DotTwo.offsetWidth / 2)) + "px";
		ColorTool.prototype.BarCalculation(event.offsetX);
	}

	ColorTool.prototype.DotOneFun = function (event) {
		ColorTool.prototype.Ele.PanelContainer.removeEventListener("click", ColorTool.prototype.panel);
		let DotOneTop = event.offsetY > 0 ? event.offsetY : 0;
		let DotOneLeft = event.offsetX > 0 ? event.offsetX : 0;
		document.onmousemove = function (e) {
			e.preventDefault();
			let PanelContainerWidth = ColorTool.prototype.Ele.PanelContainer.clientWidth;
			let PanelContainerHeight = ColorTool.prototype.Ele.PanelContainer.clientHeight;
			let OneTop = e.clientY - (ColorTool.prototype.Ele.ColorToolContainer.offsetTop + DotOneTop);
			let OneLeft = e.clientX - (ColorTool.prototype.Ele.ColorToolContainer.offsetLeft + DotOneLeft);
			OneTop = OneTop <= -(ColorTool.prototype.Ele.DotOne.offsetHeight / 2) ? -(ColorTool.prototype.Ele.DotOne.offsetHeight / 2) + 1 : OneTop;
			OneTop = OneTop >= (PanelContainerHeight - (ColorTool.prototype.Ele.DotOne.offsetHeight / 2)) ? (PanelContainerHeight - (ColorTool.prototype.Ele.DotOne.offsetHeight / 2)) : OneTop;
			OneLeft = OneLeft <= -(ColorTool.prototype.Ele.DotOne.offsetWidth / 2) ? -(ColorTool.prototype.Ele.DotOne.offsetWidth / 2) : OneLeft;
			OneLeft = OneLeft >= (PanelContainerWidth - (ColorTool.prototype.Ele.DotOne.offsetWidth / 2)) ? (PanelContainerWidth - (ColorTool.prototype.Ele.DotOne.offsetWidth / 2)) : OneLeft;
			ColorTool.prototype.Ele.DotOne.style.top = OneTop + "px";
			ColorTool.prototype.Ele.DotOne.style.left = OneLeft + "px";
			ColorTool.prototype.PanelCalculation((e.clientX - ColorTool.prototype.Ele
				.ColorToolContainer.offsetLeft), (e.clientY - ColorTool.prototype.Ele.ColorToolContainer.offsetTop));
		}
		document.onmouseup = function (e) {
			document.onmousemove = null;
			document.onmouseup = null;
			let delayed = setTimeout(function () {
				ColorTool.prototype.Ele.PanelContainer.addEventListener('click', ColorTool.prototype.panel);
				clearTimeout(delayed);
			}, 10);
		}
	}

	ColorTool.prototype.DotTwoFun = function (event) {
		ColorTool.prototype.Ele.Discoloration.removeEventListener("click", ColorTool.prototype
			.DiscolorationFun);
		let DotTwoLeft = event.offsetX > 0 ? event.offsetX : 0;
		document.onmousemove = function (e) {
			e.preventDefault();
			let TwoLeft = e.clientX - (ColorTool.prototype.Ele.ColorToolContainer.offsetLeft + DotTwoLeft);
			let DiscolorationWidth = ColorTool.prototype.Ele.Discoloration.clientWidth;
			TwoLeft = TwoLeft <= -(ColorTool.prototype.Ele.DotTwo.offsetWidth / 2) ? -(ColorTool.prototype.Ele.DotTwo.offsetWidth / 2) : TwoLeft;

			TwoLeft = TwoLeft >= (DiscolorationWidth - (ColorTool.prototype.Ele.DotTwo.offsetWidth / 2)) ? (DiscolorationWidth - (ColorTool.prototype.Ele.DotTwo.offsetWidth / 2)) : TwoLeft;
			ColorTool.prototype.Ele.DotTwo.style.left = TwoLeft + "px";
			ColorTool.prototype.BarCalculation(e.clientX - ColorTool.prototype.Ele.ColorToolContainer
				.offsetLeft);
		}
		document.onmouseup = function (e) {
			document.onmousemove = null;
			document.onmouseup = null;
			let delayed = setTimeout(function () {
				ColorTool.prototype.Ele.Discoloration.addEventListener('click', ColorTool
					.prototype.DiscolorationFun);
				clearTimeout(delayed);
			}, 10);
		}
	}

	ColorTool.prototype.setDistance = function (hsb) {
		ColorTool.prototype.Ele.DotOne.style.top = parseInt(((100 - hsb.b) * 124 / 100) - (ColorTool.prototype.Ele.DotOne.offsetHeight / 2)) + "px";
		ColorTool.prototype.Ele.DotOne.style.left = parseInt((hsb.s * 216 / 100) - (ColorTool.prototype.Ele.DotOne.offsetWidth / 2)) + "px";
		ColorTool.prototype.Ele.DotTwo.style.left = parseInt((hsb.h / 360 * 216) - (ColorTool.prototype.Ele.DotTwo.offsetWidth / 2)) + "px";
		this.PanelCalculation(hsb.s * 216 / 100, (100 - hsb.b) * 124 / 100);
		this.BarCalculation(hsb.h / 360 * 216);
	}

	ColorTool.prototype.SpecialSymbol = function (str) {
		var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？ ]");
		if (pattern.test(str)) {
			return true;
		}
		return false;
	}

	ColorTool.prototype.HexInput = function (Blur) {
		let ColorToolThis = ColorTool.prototype;
		if (!ColorTool.prototype.Ele.input[0].value) {
			return null;
		}
		if (ColorTool.prototype.Ele.input[0].value[0] !== "#") {
			ThisValuePro = "#" + ColorTool.prototype.Ele.input[0].value;
		} else {
			ThisValuePro = ColorTool.prototype.Ele.input[0].value.substring(1);
		}

		if (!ColorToolThis.SpecialSymbol(ThisValuePro)) {
			if (ThisValuePro.length === 6) {
				ColorToolThis.setDistance(ColorToolThis.hexToHsb(ColorTool.prototype.Ele.input[0].value));
			} else if(Blur){
				if (ThisValuePro.length === 3) {
					let NewValue = ThisValuePro[0] + ThisValuePro[0] + ThisValuePro[1] + ThisValuePro[1] +
						ThisValuePro[
						2] + ThisValuePro[2];
					ColorToolThis.setDistance(ColorToolThis.hexToHsb(NewValue));
				}
			}
		}
	}

	ColorTool.prototype.RgbInput = function (event) {
		let ColorToolThis = ColorTool.prototype;
		if (!ColorToolThis.SpecialSymbol(ColorToolThis.Ele.input[1].value) && !ColorToolThis
			.SpecialSymbol(ColorToolThis.Ele.input[2].value) &&
			!ColorToolThis.SpecialSymbol(ColorToolThis.Ele.input[3].value)) {
			if (parseInt(ColorToolThis.Ele.input[1].value) >= 0 && parseInt(ColorToolThis.Ele.input[2]
				.value) >= 0 && parseInt(ColorToolThis.Ele.input[3].value) >=
				0) {
				if (parseInt(ColorToolThis.Ele.input[1].value) <= 255 && parseInt(ColorToolThis.Ele.input[2]
					.value) <= 255 && parseInt(ColorToolThis.Ele.input[3]
						.value) <=
					255) {
					ColorToolThis.setDistance(ColorToolThis.rgbToHsb({
						r: parseInt(ColorToolThis.Ele.input[1].value),
						g: parseInt(ColorToolThis.Ele.input[2].value),
						b: parseInt(ColorToolThis.Ele.input[3].value)
					}));
				}
			}
		}
		ColorTool.prototype.hslToRgb(0, 100, 50);
	}

	ColorTool.prototype.HslInput = function (event) {
		let ColorToolThis = ColorTool.prototype;
		if (!ColorToolThis.SpecialSymbol(ColorToolThis.Ele.input[4].value) && !ColorToolThis
			.SpecialSymbol(ColorToolThis.Ele.input[5].value) &&
			!ColorToolThis.SpecialSymbol(ColorToolThis.Ele.input[6].value)) {
			if (parseInt(ColorToolThis.Ele.input[4].value) >= 0 && parseInt(ColorToolThis.Ele.input[5]
				.value) >= 0 && parseInt(ColorToolThis.Ele.input[6].value) >=
				0) {
				if (parseInt(ColorToolThis.Ele.input[4].value) <= 360 && parseInt(ColorToolThis.Ele.input[5]
					.value) <= 100 && parseInt(ColorToolThis.Ele.input[6]
						.value) <=
					100) {
					ColorToolThis.setDistance(
						ColorToolThis.rgbToHsb(
							ColorTool.prototype.hslToRgb(
								parseInt(ColorToolThis.Ele.input[4].value),
								parseInt(ColorToolThis.Ele.input[5].value),
								parseInt(ColorToolThis.Ele.input[6].value)
							)
						)
					);
				}
			}
		}
	}

	ColorTool.prototype.NoneOrFlex = function (constant) {
		ColorTool.prototype.Ele.InputBoxOne.style.display = "none";
		ColorTool.prototype.Ele.InputBoxTwo.style.display = "none";
		ColorTool.prototype.Ele.InputBoxThree.style.display = "none";
		if (constant === 0) {
			ColorTool.prototype.Ele.InputBoxOne.style.display = "block";
		} else if (constant === 1) {
			ColorTool.prototype.Ele.InputBoxTwo.style.display = "flex";
		} else {
			ColorTool.prototype.Ele.InputBoxThree.style.display = "flex";
		}
	}

	ColorTool.prototype.EventCollection = function () {
		ColorTool.prototype.Ele.PanelContainer.addEventListener('click', ColorTool.prototype.panel);
		ColorTool.prototype.Ele.Discoloration.addEventListener('click', ColorTool.prototype
			.DiscolorationFun);
		ColorTool.prototype.Ele.DotOne.addEventListener('mousedown', ColorTool.prototype.DotOneFun);
		ColorTool.prototype.Ele.DotTwo.addEventListener('mousedown', ColorTool.prototype.DotTwoFun);
		ColorTool.prototype.Ele.input[0].addEventListener('input', function(){
			ColorTool.prototype.HexInput(false)
		});

		ColorTool.prototype.Ele.input[0].addEventListener('blur', function(){
			ColorTool.prototype.HexInput(true)
		});
		ColorTool.prototype.Ele.input[1].addEventListener('input', ColorTool.prototype.RgbInput);
		ColorTool.prototype.Ele.input[2].addEventListener('input', ColorTool.prototype.RgbInput);
		ColorTool.prototype.Ele.input[3].addEventListener('input', ColorTool.prototype.RgbInput);
		ColorTool.prototype.Ele.input[4].addEventListener('input', ColorTool.prototype.HslInput);
		ColorTool.prototype.Ele.input[5].addEventListener('input', ColorTool.prototype.HslInput);
		ColorTool.prototype.Ele.input[6].addEventListener('input', ColorTool.prototype.HslInput);
		ColorTool.prototype.Ele.ControlButton.addEventListener('click', function () {
			ColorTool.prototype.init(Initial);
		});

		ColorTool.prototype.Ele.LeftAndRightlSpan[0].addEventListener('click', function () {
			ColorTool.prototype.Ele.constant = --ColorTool.prototype.Ele.constant >= 0 ? --ColorTool.prototype.Ele.constant : 1;
			ColorTool.prototype.NoneOrFlex(++ColorTool.prototype.Ele.constant);
		});
		
		ColorTool.prototype.Ele.LeftAndRightlSpan[1].addEventListener('click', function () {
			ColorTool.prototype.Ele.constant = ++ColorTool.prototype.Ele.constant <= 2 ? ++ColorTool.prototype.Ele.constant : 1;
			ColorTool.prototype.NoneOrFlex(--ColorTool.prototype.Ele.constant);
		});
	}

	event.ColorTool = ColorTool;
})(window);