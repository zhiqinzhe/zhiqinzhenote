//创建页面函数
function createPage() {
    const CounterfeitBody = $('<div id="CounterfeitBody" class="CounterfeitBodyClass"></div>'),
        BodyMain = $('<div id="BodyMain"></div>'),
        sample = $('<div id="sample"></div>'),
        ColorToolContainer = $('<div id="ColorToolContainer"></div>'),
        PanelContainer = $('<div id="PanelContainer"></div>'),
        panelCanvas = $('<canvas id="panelCanvas" width="216" height="124">Your browser does not support canvas</canvas>'),
        DotOne = $('<div id="DotOne"></div>'),
        BarContainer = $('<div id="BarContainer"></div>'),
        Discoloration = $('<div id="Discoloration"></div>'),
        DotTwo = $('<div id="DotTwo"></div>'),
        other = $('<div id="other"></div>'),
        InputBoxContainer = $('<div id="InputBoxContainer"></div>'),
        InputBoxOne = $('<div id="InputBoxOne"></div>'),
        InputBoxOneinput = $('<input value="#ff0000" spellcheck="false" maxlength="7">'),
        InputBoxOnespan = $('<span>HEX（十六进制）</span>'),
        InputBoxTwo = $('<div id="InputBoxTwo"></div>'),
        InputBoxTwoDivR = $('<div></div>'),
        InputBoxTwoDivRinput = $('<input id="colorpicker-hexInput-one" value="255" maxlength="3" spellcheck="false">'),
        InputBoxTwoDivRspan = $('<span>r</span>'),
        InputBoxTwoDivG = $('<div></div>'),
        InputBoxTwoDivGinput = $('<input id="colorpicker-hexInput-two" value="0" maxlength="3" spellcheck="false">'),
        InputBoxTwoDivGspan = $('<span>g</span>'),
        InputBoxTwoDivB = $('<div></div>'),
        InputBoxTwoDivBinput = $('<input id="colorpicker-hexInput-three" value="0" maxlength="3" spellcheck="false">'),
        InputBoxTwoDivBspan = $('<span>b</span>'),
        InputBoxThree = $('<div id="InputBoxThree"></div>'),
        InputBoxThreeDivH = $('<div></div>'),
        InputBoxThreeDivHinput = $('<input id="colorpicker-hslInput-one" value="255" maxlength="4" spellcheck="false">'),
        InputBoxThreeDivHspan = $('<span>h</span>'),
        InputBoxThreeDivS = $('<div></div>'),
        InputBoxThreeDivSinput = $('<input id="colorpicker-hslInput-two" value="0" maxlength="4" spellcheck="false">'),
        InputBoxThreeDivSpan = $('<span>s</span>'),
        InputBoxThreeDivL = $('<div></div>'),
        InputBoxThreeDivLinput = $('<input id="colorpicker-hslInput-three" value="0" maxlength="4" spellcheck="false">'),
        InputBoxThreeDivLpan = $('<span>l</span>'),
        control = $('<div id="control"></div>'),
        controlbutton = $('<button>重置</button>'),
        LeftAndRight = $('<div id="LeftAndRight"></div>'),
        LeftAndRightleftspan = $('<span id="left"></span>'),
        LeftAndRightleftspani = $('<i><svg xmlns="http://www.w3.org/2000/svg" style="fill:#969696" version="1.1" width="12" height="12" viewBox="0 0 1024 1024"><g id="icomoon-ignore"></g><path xmlns="http://www.w3.org/2000/svg" id="st0" d="M768,63.2c0,0.1,0,0.3,0,0.4c0,16.5-6.2,31.5-16.4,42.9l0.1-0.1L405.2,494.8c-4,4.5-6.5,10.5-6.5,17  c0,6.5,2.5,12.5,6.5,17l0,0l346.4,388.4c10,11.4,16.2,26.4,16.2,42.9c0,20.9-9.9,39.5-25.2,51.4l-0.1,0.1c-10.3,7.5-23.3,12-37.3,12  c-19.5,0-36.9-8.7-48.6-22.4l-0.1-0.1L281.6,581.1c-16.5-18.3-26.5-42.6-26.5-69.2s10.1-51,26.6-69.3l-0.1,0.1L656.9,22.8  c11.9-13.9,29.4-22.6,48.9-22.6c15.1,0,29.1,5.2,40,14l-0.1-0.1C759.4,26,768,43.4,768,62.8C768,63,768,63.1,768,63.2L768,63.2  L768,63.2z"/></svg></i>'),
        LeftAndRightrightspan = $('<span id="right"></span>'),
        LeftAndRightrightspani = $('<i><svg xmlns="http://www.w3.org/2000/svg" style="fill:#969696" version="1.1" width="12" height="12" viewBox="0 0 1024 1024"><g id="icomoon-ignore"></g><path xmlns="http://www.w3.org/2000/svg" id="st0" d="M256,63.2c0,0.1,0,0.3,0,0.4c0,16.5,6.2,31.5,16.4,42.9l-0.1-0.1l346.4,388.4c4,4.5,6.5,10.5,6.5,17  c0,6.5-2.5,12.5-6.5,17l0,0L272.4,917.2c-10,11.4-16.2,26.4-16.2,42.9c0,20.9,9.9,39.5,25.2,51.4l0.1,0.1c10.4,7.5,23.3,12,37.3,12  c19.5,0,36.9-8.7,48.6-22.4l0.1-0.1l374.8-420.1c16.5-18.3,26.5-42.6,26.5-69.2s-10.1-51-26.6-69.3l0.1,0.1L367.1,22.8  C355.2,8.9,337.7,0.2,318.2,0.2c-15.1,0-29.1,5.2-40,14l0.1-0.1C264.6,26,256,43.4,256,62.8C256,63,256,63.1,256,63.2L256,63.2  L256,63.2z"/></svg></i>'),
        getColor = $('<button id="getColor">吸取颜色</button>'),
        MSclose = $('<button id="MSclose">收起来</button>');
    CounterfeitBody.append(BodyMain);
    BodyMain.append(sample);
    PanelContainer.append(DotOne);
    PanelContainer.append(panelCanvas);
    ColorToolContainer.append(PanelContainer);
    Discoloration.append(DotTwo);
    BarContainer.append(Discoloration);
    ColorToolContainer.append(BarContainer);
    InputBoxOne.append(InputBoxOneinput);
    InputBoxOne.append(InputBoxOnespan);
    InputBoxContainer.append(InputBoxOne);
    InputBoxTwoDivR.append(InputBoxTwoDivRinput);
    InputBoxTwoDivR.append(InputBoxTwoDivRspan);
    InputBoxTwo.append(InputBoxTwoDivR);
    InputBoxTwoDivG.append(InputBoxTwoDivGinput);
    InputBoxTwoDivG.append(InputBoxTwoDivGspan);
    InputBoxTwo.append(InputBoxTwoDivG);
    InputBoxTwoDivB.append(InputBoxTwoDivBinput);
    InputBoxTwoDivB.append(InputBoxTwoDivBspan);
    InputBoxTwo.append(InputBoxTwoDivB);
    InputBoxContainer.append(InputBoxTwo);
    InputBoxThreeDivH.append(InputBoxThreeDivHinput);
    InputBoxThreeDivH.append(InputBoxThreeDivHspan);
    InputBoxThree.append(InputBoxThreeDivH);
    InputBoxThreeDivS.append(InputBoxThreeDivSinput);
    InputBoxThreeDivS.append(InputBoxThreeDivSpan);
    InputBoxThree.append(InputBoxThreeDivS);
    InputBoxThreeDivL.append(InputBoxThreeDivLinput);
    InputBoxThreeDivL.append(InputBoxThreeDivLpan);
    InputBoxThree.append(InputBoxThreeDivL);
    InputBoxContainer.append(InputBoxThree);
    other.append(InputBoxContainer);
    control.append(controlbutton);
    LeftAndRightleftspan.append(LeftAndRightleftspani);
    LeftAndRight.append(LeftAndRightleftspan);
    LeftAndRightrightspan.append(LeftAndRightrightspani);
    LeftAndRight.append(LeftAndRightrightspan);
    control.append(LeftAndRight);
    other.append(control);
    ColorToolContainer.append(other);
    BodyMain.append(ColorToolContainer);
    BodyMain.append(getColor);
    BodyMain.append(MSclose);
    const openColorPanel = $('<div id="openColorPanel" class="openColorPanelClass">拾取颜色</div>');

    const colorData = localStorage.getItem("ColorToolStorage") || "#255696";

    function appearCheckedTrue() {
        $('body').append(CounterfeitBody);
        $('body').append(openColorPanel);
        $('#openColorPanel').click(function () {
            $('#openColorPanel').each(function () {
                $(this).addClass('EndOpenColorPanel');
            });
            $('#CounterfeitBody').css('display', 'flex');
            // $('#openColorPanel').css('display', 'none');
            //拖拽
            drag();
            let setTime = setTimeout(function () {
                $('#openColorPanel').css('display', 'none');
                $('#openColorPanel').removeClass('EndOpenColorPanel');
                clearTimeout(setTime);
            }, 300);
        });

        $('#MSclose').click(function () {
            $('#CounterfeitBody').each(function () {
                $(this).addClass('EndCounterfeitBodyClass');
            });
            $('#openColorPanel').css('display', 'block');
            let setTime = setTimeout(function () {
                $('#CounterfeitBody').css('display', 'none');
                $('#CounterfeitBody').removeClass('EndCounterfeitBodyClass');
                clearTimeout(setTime);
            }, 300);
        });
        ColorTool(colorData).then(function (ColorData) { });
    }

    chrome.storage.sync.get(['appearChecked'], function (result) {
        if (result.appearChecked) {
            appearCheckedTrue();
        }
    });

    chrome.storage.onChanged.addListener(
        function (changes, namespace) {
            for (let [key, { oldValue, newValue }] of Object.entries(changes)) {
                if (key === "appearChecked" && namespace === "sync" && (newValue === true || newValue === false)) {
                    if (newValue === true) {
                        appearCheckedTrue();
                    } else {
                        $('#openColorPanel').removeAttr('style');
                        $('#CounterfeitBody').removeAttr('style');
                        $("#openColorPanel").remove();
                        $('#CounterfeitBody').remove();
                    }
                }
                //存储的名字
                // console.log(key)
                //存储的类型local或者sync
                // console.log(namespace)
                //存储更新前的数据,首次存储为undefined
                // console.log(oldValue)
                //存储更新后的数据
                // console.log(newValue)
            }
        });
}
createPage();

//拖拽
function drag(e) {
    let oldX, oldY, newX, newY,
        sampleEle = document.querySelector('#CounterfeitBody #sample'),
        CounterfeitBodyEle = document.querySelector('#CounterfeitBody');
    sampleEle.onmousedown = function (e) {
        if (!CounterfeitBodyEle.style.right && !CounterfeitBodyEle.style.bottom) {
            CounterfeitBodyEle.style.right = 0
            CounterfeitBodyEle.style.bottom = 0
        }
        oldX = e.clientX
        oldY = e.clientY
        document.onmousemove = function (e) {
            newX = e.clientX
            newY = e.clientY
            CounterfeitBodyEle.style.right = parseInt(CounterfeitBodyEle.style.right) - newX + oldX + 'px'
            CounterfeitBodyEle.style.bottom = parseInt(CounterfeitBodyEle.style.bottom) - newY + oldY + 'px'
            oldX = newX
            oldY = newY
        }
        document.onmouseup = function () {
            document.onmousemove = null
            document.onmouseup = null
        }
    }
}