package com.example.fileupload.servie;

import com.example.fileupload.bean.FilePO;

import java.util.List;

public interface FileService {
    Integer addFile(FilePO filePO);

    Boolean selectFileByMd5(String md5);

    List<FilePO> selectFileList();
}
