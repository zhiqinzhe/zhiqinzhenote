package com.example.fileupload.dao;

import com.example.fileupload.bean.FilePO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FileMapper {
    public Integer insertFile(FilePO filePO) ;

    FilePO selectFileByMd5(String md5);

    List<FilePO> selectFileList();
}
