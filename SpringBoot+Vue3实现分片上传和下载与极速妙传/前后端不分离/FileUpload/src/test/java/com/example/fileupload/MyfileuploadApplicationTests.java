package com.example.fileupload;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MyfileuploadApplicationTests {

    @Test
    void contextLoads() {
        String[] splits = "a.jpg".split("\\.");
        System.out.println(splits.length);
        for (String split : splits) {
            System.out.println(split);
        }
    }

}
