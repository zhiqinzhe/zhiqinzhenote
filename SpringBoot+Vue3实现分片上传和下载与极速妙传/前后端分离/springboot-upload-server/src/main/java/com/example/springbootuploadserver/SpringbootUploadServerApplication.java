package com.example.springbootuploadserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootUploadServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootUploadServerApplication.class, args);
        System.out.println("项目启动成功o(*￣▽￣*)ブ");
    }

}
