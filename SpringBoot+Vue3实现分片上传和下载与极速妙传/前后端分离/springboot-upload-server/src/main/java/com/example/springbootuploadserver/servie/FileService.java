package com.example.springbootuploadserver.servie;

import com.example.springbootuploadserver.bean.FilePO;

import java.util.List;

public interface FileService {
    Integer addFile(FilePO filePO);

    Boolean selectFileByMd5(String md5);

    List<FilePO> selectFileList();
}
